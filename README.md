Just Another Magazine is an online magazine covering wellbeing, relationships, travel, food, work and anything else that matters to us and to you.

Website: https://www.justanothermagazine.com/
